import {data} from "./inputTrips"
import fs from "fs"

let list = [];

// get all flights list
data.forEach(item=>{
    list=[...list,...item.user.flights]
})

// change key name
list = list.map(item=> ({
    latitude: item.arrivalCityLatitude,
    longitude: item.arrivalCityLongitude,
    city: item.arrivalName,
    country: item.arrivalCountry,
    flights:[]

}))

// remove duplicate in list
let uniqueList = Array.from(new Set(list.map(a => a.city)))
    .map(city => {
        return list.find(a => a.city === city)
    })

// filter FlightsUser
const GetFlightsUser = (item)=>{
    let FlightsUser = []
    data.forEach(d=>{
       d.user.flights.forEach(filterData=>{
            if(item.city === filterData.arrivalName){
                FlightsUser.push({
                    user: {
                        "id": d.user.id,
                        "firstName": d.user.firstname,
                        "email": d.user.email
                    },
                    flight: filterData
                })
            }
        })
    })

    return FlightsUser
}

uniqueList = uniqueList.map(item=>({
    ...item,
    flights:GetFlightsUser(item)
}))

console.log(uniqueList)

//convert to file and save
fs.writeFile("out.json", JSON.stringify(uniqueList), function(err) {
    if (err) {
        console.log(err);
    }
});