"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inputTrips_1 = require("./inputTrips");
const fs_1 = __importDefault(require("fs"));
let list = [];
inputTrips_1.data.forEach(item => {
    list = [...list, ...item.user.flights];
});
list = list.map(item => ({
    latitude: item.arrivalCityLatitude,
    longitude: item.arrivalCityLongitude,
    city: item.arrivalName,
    country: item.arrivalCountry,
    flights: []
}));
let uniqueList = Array.from(new Set(list.map(a => a.city)))
    .map(city => {
    return list.find(a => a.city === city);
});
const GetFlightsUser = (item) => {
    let FlightsUser = [];
    inputTrips_1.data.forEach(d => {
        d.user.flights.forEach(filterData => {
            if (item.city === filterData.arrivalName) {
                FlightsUser.push({
                    user: {
                        "id": d.user.id,
                        "firstName": d.user.firstname,
                        "email": d.user.email
                    },
                    flight: filterData
                });
            }
        });
    });
    return FlightsUser;
};
uniqueList = uniqueList.map(item => (Object.assign(Object.assign({}, item), { flights: GetFlightsUser(item) })));
console.log(uniqueList);
fs_1.default.writeFile("out.json", JSON.stringify(uniqueList), function (err) {
    if (err) {
        console.log(err);
    }
});
//# sourceMappingURL=app.js.map