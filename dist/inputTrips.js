"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.data = void 0;
exports.data = [
    {
        "user": {
            "id": 47403,
            "firstname": "test5",
            "lastName": "acmecompanyEurope",
            "email": "test5acmecompanyeurope@gmail.com",
            "flights": [
                {
                    "id": 167219,
                    "departureDateTimeUtc": "2021-06-10T12:30:00.000Z",
                    "departureDateTime": "2021-06-10T18:00:00.000Z",
                    "departureAirport": "BOM",
                    "departureName": "Mumbai",
                    "departureCityLatitude": 19.088699340820312,
                    "departureCityLongitude": 72.8661117553711,
                    "departureCountry": "IN",
                    "arrivalDateTimeUtc": "2021-06-11T16:55:00.000Z",
                    "arrivalDateTime": "2021-06-11T22:25:00.000Z",
                    "arrivalAirport": "DEL",
                    "arrivalName": "Delhi",
                    "arrivalCityLatitude": 28.566499710083008,
                    "arrivalCityLongitude": 77.11213684082031,
                    "arrivalCountry": "IN",
                    "operatingAirline": "SG",
                    "flightNumber": "318",
                    "confirmationNumber": "EX0001"
                }
            ]
        }
    },
    {
        "user": {
            "id": 47405,
            "firstname": "test3",
            "lastName": "acmecompanyUK",
            "email": "test3acmecompanyuk@gmail.com",
            "flights": [
                {
                    "id": 167208,
                    "departureDateTimeUtc": "2021-06-24T18:00:00.000Z",
                    "departureDateTime": "2021-06-25T02:00:00.000Z",
                    "departureAirport": "PVG",
                    "departureName": "Shanghai",
                    "departureCityLatitude": 31.222219467163086,
                    "departureCityLongitude": 121.45806121826172,
                    "departureCountry": "CN",
                    "arrivalDateTimeUtc": "2021-06-26T11:30:00.000Z",
                    "arrivalDateTime": "2021-06-26T04:30:00.000Z",
                    "arrivalAirport": "LAX",
                    "arrivalName": "Los Angeles",
                    "arrivalCityLatitude": 33.942501068115234,
                    "arrivalCityLongitude": -118.40807342529297,
                    "arrivalCountry": "US",
                    "operatingAirline": "Y8",
                    "flightNumber": "7455",
                    "confirmationNumber": "EX0002"
                }
            ]
        }
    },
    {
        "user": {
            "id": 47406,
            "firstname": "test4",
            "lastName": "acmecompanyUK",
            "email": "test4acmecompanyuk@gmail.com",
            "flights": [
                {
                    "id": 167213,
                    "departureDateTimeUtc": "2021-05-24T18:00:00.000Z",
                    "departureDateTime": "2021-05-25T02:00:00.000Z",
                    "departureAirport": "PVG",
                    "departureName": "Shanghai",
                    "departureCityLatitude": 31.222219467163086,
                    "departureCityLongitude": 121.45806121826172,
                    "departureCountry": "CN",
                    "arrivalDateTimeUtc": "2021-05-25T23:00:00.000Z",
                    "arrivalDateTime": "2021-05-26T04:30:00.000Z",
                    "arrivalAirport": "DEL",
                    "arrivalName": "Delhi",
                    "arrivalCityLatitude": 28.566499710083008,
                    "arrivalCityLongitude": 77.11213684082031,
                    "arrivalCountry": "IN",
                    "operatingAirline": "AC",
                    "flightNumber": "7878",
                    "confirmationNumber": "EX0003"
                },
                {
                    "id": 167214,
                    "departureDateTimeUtc": "2021-06-27T03:00:00.000Z",
                    "departureDateTime": "2021-06-26T20:00:00.000Z",
                    "departureAirport": "LAX",
                    "departureName": "Los Angeles",
                    "departureCityLatitude": 33.942501068115234,
                    "departureCityLongitude": -118.40807342529297,
                    "departureCountry": "US",
                    "arrivalDateTimeUtc": "2021-06-27T16:55:00.000Z",
                    "arrivalDateTime": "2021-06-27T22:25:00.000Z",
                    "arrivalAirport": "DEL",
                    "arrivalName": "Delhi",
                    "arrivalCityLatitude": 28.566499710083008,
                    "arrivalCityLongitude": 77.11213684082031,
                    "arrivalCountry": "IN",
                    "operatingAirline": "DW",
                    "flightNumber": "8989",
                    "confirmationNumber": "EX0004"
                }
            ]
        }
    },
    {
        "user": {
            "id": 47407,
            "firstname": "Katie",
            "lastName": "Hughes",
            "email": "test1acmecompanytbg@gmail.com",
            "flights": [
                {
                    "id": 169485,
                    "departureDateTimeUtc": "2021-06-25T05:45:00.000Z",
                    "departureDateTime": "2021-06-25T11:15:00.000Z",
                    "departureAirport": "HYD",
                    "departureName": "Hyderabad",
                    "departureCityLatitude": 17.2313175201416,
                    "departureCityLongitude": 78.45808410644531,
                    "departureCountry": "IN",
                    "arrivalDateTimeUtc": "2021-06-26T07:45:00.000Z",
                    "arrivalDateTime": "2021-06-26T13:15:00.000Z",
                    "arrivalAirport": "PNQ",
                    "arrivalName": "Pune",
                    "arrivalCityLatitude": 18.58209991455078,
                    "arrivalCityLongitude": 73.91999816894531,
                    "arrivalCountry": "IN",
                    "operatingAirline": "G8",
                    "flightNumber": "515",
                    "confirmationNumber": "EX0005"
                }
            ]
        }
    },
    {
        "user": {
            "id": 47407,
            "firstname": "Katie",
            "lastName": "Hughes",
            "email": "test1acmecompanytbg@gmail.com",
            "flights": [
                {
                    "id": 169492,
                    "departureDateTimeUtc": "2021-07-25T02:15:00.000Z",
                    "departureDateTime": "2021-07-25T11:15:00.000Z",
                    "departureAirport": "ICN",
                    "departureName": "Seoul",
                    "departureCityLatitude": 37.46910095214844,
                    "departureCityLongitude": 126.43916320800781,
                    "departureCountry": "KR",
                    "arrivalDateTimeUtc": "2021-07-26T20:15:00.000Z",
                    "arrivalDateTime": "2021-07-26T13:15:00.000Z",
                    "arrivalAirport": "LAX",
                    "arrivalName": "Los Angeles",
                    "arrivalCityLatitude": 33.942501068115234,
                    "arrivalCityLongitude": -118.40807342529297,
                    "arrivalCountry": "US",
                    "operatingAirline": "9S",
                    "flightNumber": "96",
                    "confirmationNumber": "EX0006"
                }
            ]
        }
    },
    {
        "user": {
            "id": 47407,
            "firstname": "Katie",
            "lastName": "Hughes",
            "email": "test1acmecompanytbg@gmail.com",
            "flights": [
                {
                    "id": 167423,
                    "departureDateTimeUtc": "2021-06-02T01:05:00.000Z",
                    "departureDateTime": "2021-06-01T20:05:00.000Z",
                    "departureAirport": "IAH",
                    "departureName": "Houston",
                    "departureCityLatitude": 29.984399795532227,
                    "departureCityLongitude": -95.34143829345703,
                    "departureCountry": "US",
                    "arrivalDateTimeUtc": "2021-06-02T05:29:00.000Z",
                    "arrivalDateTime": "2021-06-01T22:29:00.000Z",
                    "arrivalAirport": "SFO",
                    "arrivalName": "San Francisco",
                    "arrivalCityLatitude": 37.61899948120117,
                    "arrivalCityLongitude": -122.37488555908203,
                    "arrivalCountry": "US",
                    "operatingAirline": "United",
                    "flightNumber": "1844",
                    "confirmationNumber": "EX0007"
                },
                {
                    "id": 167424,
                    "departureDateTimeUtc": "2021-06-07T23:00:00.000Z",
                    "departureDateTime": "2021-06-07T16:00:00.000Z",
                    "departureAirport": "SFO",
                    "departureName": "San Francisco",
                    "departureCityLatitude": 37.61899948120117,
                    "departureCityLongitude": -122.37488555908203,
                    "departureCountry": "US",
                    "arrivalDateTimeUtc": "2021-06-08T02:47:00.000Z",
                    "arrivalDateTime": "2021-06-07T21:47:00.000Z",
                    "arrivalAirport": "IAH",
                    "arrivalName": "Houston",
                    "arrivalCityLatitude": 29.984399795532227,
                    "arrivalCityLongitude": -95.34143829345703,
                    "arrivalCountry": "US",
                    "operatingAirline": "United",
                    "flightNumber": "1878",
                    "confirmationNumber": "EX0008"
                }
            ]
        }
    },
    {
        "user": {
            "id": 47407,
            "firstname": "Katie",
            "lastName": "Hughes",
            "email": "test1acmecompanytbg@gmail.com",
            "flights": [
                {
                    "id": 167425,
                    "departureDateTimeUtc": "2021-08-01T15:20:00.000Z",
                    "departureDateTime": "2021-08-01T10:20:00.000Z",
                    "departureAirport": "IAH",
                    "departureName": "Houston",
                    "departureCityLatitude": 29.984399795532227,
                    "departureCityLongitude": -95.34143829345703,
                    "departureCountry": "US",
                    "arrivalDateTimeUtc": "2021-08-02T06:30:00.000Z",
                    "arrivalDateTime": "2021-08-02T15:30:00.000Z",
                    "arrivalAirport": "NRT",
                    "arrivalName": "Tokyo",
                    "arrivalCityLatitude": 35.76470184326172,
                    "arrivalCityLongitude": 140.38638305664062,
                    "arrivalCountry": "JP",
                    "operatingAirline": "United",
                    "flightNumber": "7",
                    "confirmationNumber": "EX0009"
                },
                {
                    "id": 167426,
                    "departureDateTimeUtc": "2021-08-03T08:15:00.000Z",
                    "departureDateTime": "2021-08-03T17:15:00.000Z",
                    "departureAirport": "NRT",
                    "departureName": "Tokyo",
                    "departureCityLatitude": 35.76470184326172,
                    "departureCityLongitude": 140.38638305664062,
                    "departureCountry": "JP",
                    "arrivalDateTimeUtc": "2021-08-03T19:00:00.000Z",
                    "arrivalDateTime": "2021-08-03T14:00:00.000Z",
                    "arrivalAirport": "IAH",
                    "arrivalName": "Houston",
                    "arrivalCityLatitude": 29.984399795532227,
                    "arrivalCityLongitude": -95.34143829345703,
                    "arrivalCountry": "US",
                    "operatingAirline": "United",
                    "flightNumber": "6",
                    "confirmationNumber": "EX0010"
                }
            ]
        }
    },
    {
        "user": {
            "id": 47408,
            "firstname": "Vetle",
            "lastName": "Aasland",
            "email": "test2acmecompanytbg@gmail.com",
            "flights": [
                {
                    "id": 167437,
                    "departureDateTimeUtc": "2021-07-02T01:05:00.000Z",
                    "departureDateTime": "2021-07-01T20:05:00.000Z",
                    "departureAirport": "IAH",
                    "departureName": "Houston",
                    "departureCityLatitude": 29.984399795532227,
                    "departureCityLongitude": -95.34143829345703,
                    "departureCountry": "US",
                    "arrivalDateTimeUtc": "2021-07-02T05:29:00.000Z",
                    "arrivalDateTime": "2021-07-01T22:29:00.000Z",
                    "arrivalAirport": "SFO",
                    "arrivalName": "San Francisco",
                    "arrivalCityLatitude": 37.61899948120117,
                    "arrivalCityLongitude": -122.37488555908203,
                    "arrivalCountry": "US",
                    "operatingAirline": "United",
                    "flightNumber": "1844",
                    "confirmationNumber": "EX0011"
                },
                {
                    "id": 167438,
                    "departureDateTimeUtc": "2021-07-07T23:00:00.000Z",
                    "departureDateTime": "2021-07-07T16:00:00.000Z",
                    "departureAirport": "SFO",
                    "departureName": "San Francisco",
                    "departureCityLatitude": 37.61899948120117,
                    "departureCityLongitude": -122.37488555908203,
                    "departureCountry": "US",
                    "arrivalDateTimeUtc": "2021-07-08T02:47:00.000Z",
                    "arrivalDateTime": "2021-07-07T21:47:00.000Z",
                    "arrivalAirport": "IAH",
                    "arrivalName": "Houston",
                    "arrivalCityLatitude": 29.984399795532227,
                    "arrivalCityLongitude": -95.34143829345703,
                    "arrivalCountry": "US",
                    "operatingAirline": "United",
                    "flightNumber": "1878",
                    "confirmationNumber": "EX0012"
                }
            ]
        }
    },
    {
        "user": {
            "id": 47408,
            "firstname": "Vetle",
            "lastName": "Aasland",
            "email": "test2acmecompanytbg@gmail.com",
            "flights": [
                {
                    "id": 169483,
                    "departureDateTimeUtc": "2021-06-25T05:45:00.000Z",
                    "departureDateTime": "2021-06-25T11:15:00.000Z",
                    "departureAirport": "HYD",
                    "departureName": "Hyderabad",
                    "departureCityLatitude": 17.2313175201416,
                    "departureCityLongitude": 78.45808410644531,
                    "departureCountry": "IN",
                    "arrivalDateTimeUtc": "2021-06-26T07:45:00.000Z",
                    "arrivalDateTime": "2021-06-26T13:15:00.000Z",
                    "arrivalAirport": "PNQ",
                    "arrivalName": "Pune",
                    "arrivalCityLatitude": 18.58209991455078,
                    "arrivalCityLongitude": 73.91999816894531,
                    "arrivalCountry": "IN",
                    "operatingAirline": "G8",
                    "flightNumber": "515",
                    "confirmationNumber": "EX0013"
                },
                {
                    "id": 169484,
                    "departureDateTimeUtc": "2021-06-28T14:30:00.000Z",
                    "departureDateTime": "2021-06-28T20:00:00.000Z",
                    "departureAirport": "PNQ",
                    "departureName": "Pune",
                    "departureCityLatitude": 18.58209991455078,
                    "departureCityLongitude": 73.91999816894531,
                    "departureCountry": "IN",
                    "arrivalDateTimeUtc": "2021-06-28T16:55:00.000Z",
                    "arrivalDateTime": "2021-06-28T22:25:00.000Z",
                    "arrivalAirport": "HYD",
                    "arrivalName": "Hyderabad",
                    "arrivalCityLatitude": 17.2313175201416,
                    "arrivalCityLongitude": 78.45808410644531,
                    "arrivalCountry": "IN",
                    "operatingAirline": "WN",
                    "flightNumber": "1827",
                    "confirmationNumber": "EX0014"
                }
            ]
        }
    },
    {
        "user": {
            "id": 47408,
            "firstname": "Vetle",
            "lastName": "Aasland",
            "email": "test2acmecompanytbg@gmail.com",
            "flights": [
                {
                    "id": 169493,
                    "departureDateTimeUtc": "2021-07-25T02:15:00.000Z",
                    "departureDateTime": "2021-07-25T11:15:00.000Z",
                    "departureAirport": "ICN",
                    "departureName": "Seoul",
                    "departureCityLatitude": 37.46910095214844,
                    "departureCityLongitude": 126.43916320800781,
                    "departureCountry": "KR",
                    "arrivalDateTimeUtc": "2021-07-26T20:15:00.000Z",
                    "arrivalDateTime": "2021-07-26T13:15:00.000Z",
                    "arrivalAirport": "LAX",
                    "arrivalName": "Los Angeles",
                    "arrivalCityLatitude": 33.942501068115234,
                    "arrivalCityLongitude": -118.40807342529297,
                    "arrivalCountry": "US",
                    "operatingAirline": "9S",
                    "flightNumber": "96",
                    "confirmationNumber": "EX0015"
                }
            ]
        }
    }
];
//# sourceMappingURL=inputTrips.js.map